AFRAME.registerComponent("bar-chart", {
    schema: {
        type: 'string',
        parse: function barChartParse(value) {
            var json = "{" + value + "}";
            var result = JSON.parse(json)
            return result;
        }
    },

    update: function() {
        var data = this.data;
        var noBars = data.bars.length;
        var maxValue = 0;
        for(var i in data.bars) {
            var bar = data.bars[i];
            maxValue=Math.max(maxValue, bar.value);
        }
        var scale = 1.25;
        var minX = -((noBars-1)/2) * scale;
        var maxY = noBars;

        for(var i in data.bars) {
            var bar = data.bars[i];
            var posX = minX + (i * scale);
            var sizeY = (maxY * (bar.value / maxValue) * scale);
            console.log(sizeY);
            var element = document.createElement("a-box");
            element.setAttribute("position", posX + " " + ((sizeY/2) - (maxY/2)) + " 0");
            element.setAttribute("scale", "1 " + sizeY + " 1");
            element.setAttribute("color", bar.color);
            this.el.appendChild(element);
        }
    }
});

// Total: 5
// 5-1/2=2
// Verschil = 4
// Pak linkerpositie, + i?
// Linkerpositie = -2
// 0 = -2 + 0;
// 1 = -2 + 1
// 2 = -2 + 2
// i: 0 1 2 3 4
// pos: -2 -1 0 1 2

// Total: 4
// 0 1 2 3 4
// Linkerpositie: -1.5
// 
// -1.5 -.5 .5 .5